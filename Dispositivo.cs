﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class Dispositivo
    {
        public int Id { get; set; }
        public string Porta { get; set; }
        public string Descricao { get; set; }
        public int UsuarioId { get; set; }
        public string ProjetoKey { get; set; }
        public string Estado { get; set; }
        public SerialPort SerialPort { get; set; }
        public void setEstado(int estado)
        {
            if (SerialPort != null)
                estado = (SerialPort.IsOpen) ? 1 : 0;
            this.Estado = Enum.GetName(typeof(EstadoDispositivo), estado);
            
        }
    }

    public enum EstadoDispositivo
    {
        Conectar = 0,
        Desconectar = 1
    }

    public static class  LoginUser
    {
        public static string Login { get; set; }
        public static string Senha
        {
            get;
            set;
        }

        public static string GerarHashMd5(string input)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
                sBuilder.Append(data[i].ToString("x2"));
            return sBuilder.ToString();
        }
    }

}
