﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace WindowsFormsApp2
{
    public partial class LoginForm : Form
    {       
        public List<Dispositivo> _dispositivos { get; set; }
        public LoginForm()
        {
            InitializeComponent();
            linkCadastroUsuario.Links.Add(0, 50, "https://google.com");
            linkPage.Links.Add(0, 50, "https://google.com");
            linkEsqueciSenha.Links.Add(0, 50, "https://google.com");
        }

        private void btEntrar_Click(object sender, EventArgs e)
        {
            SerialRequest.Entrar(textBoxUsuario.Text, textBoxSenha.Text);
            this.Close();
        }

        private void linkEsqueciSenha_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString());
        }
    }
}