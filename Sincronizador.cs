﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using WindowsFormsApp2.Localizacoes;

namespace WindowsFormsApp2
{
    public partial class Sincronizador : Form
    {
        string RxString;
        List<Dispositivo> _dispositivos;
        private Dispositivo _dispositivo;

        public Sincronizador()
        {
            InitializeComponent();
            var form = new LoginForm();
            form.ShowDialog(Parent);
            busca_de_portas();
            preencherGrid();
        }

        private void preencherGrid()
        {
            try
            {
                _dispositivos = SerialRequest.ObterDispositivos();
                _dispositivos.ForEach(c => c.setEstado(0));
                GridViewDispositivos.DataSource = _dispositivos;
            }
            catch (Exception ex)
            {
                ExibirMensagemErro(ex.Message);
            }
        }
        void busca_de_portas()
        {
            var portas = SerialPort.GetPortNames();
            comboBoxPortas.Items.Clear();
            for (int i = 0; i < portas.Length; i++)
            {
                comboBoxPortas.Items.Add(portas[i]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            busca_de_portas();
        }
        private void btAdicionar_Click(object sender, EventArgs e)
        {

        }
        private bool Conectar(Dispositivo dispositivo)
        {
            if (dispositivo.SerialPort == null)
                dispositivo.SerialPort = new SerialPort(dispositivo.Porta);

            if (dispositivo.SerialPort.IsOpen)
                return true;
            dispositivo.SerialPort.PortName = dispositivo.Porta;
            dispositivo.SerialPort.Open();
            //dispositivo.SerialPort.BaudRate = 9200;
            dispositivo.SerialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort1_DataReceived);
            return true;            
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {        //fecha a porta
        }

        private void btEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (buttonSalvar.Text == "Adicionar")
                {
                    var device = new Dispositivo();
                    device.Descricao = textBoxDispositivo.Text;
                    if (comboBoxPortas.SelectedIndex < 0)
                        throw new Exception("Por favor selecione à porta serial do dispositivo!");
                    device.Porta = comboBoxPortas.Items[comboBoxPortas.SelectedIndex].ToString();
                    device.ProjetoKey = textBoxChave.Text;
                    SerialRequest.EditarDispositivo(device);
                    _dispositivos.Add(device);
                    ExibirMensagemSucesso(MensagemServico.GetMessage(MensagemSistemaEnum.DeviceAddedSuccessfully));
                    AtualizarGrid();
                }
                else
                {
                    SerialRequest.EditarDispositivo(_dispositivo);
                    ExibirMensagemSucesso(MensagemServico.GetMessage(MensagemSistemaEnum.DeviceUpdatedSuccessfully));
                }
                Reset();
            }
            catch (Exception ex)
            {
                ExibirMensagemErro(ex.Message);
            }
        }

        private void trataDadoRecebido(Dispositivo dispositivo)
        {
            var resposta = RxString; //SerialRequest.Set(RxString);
            EnviarData(dispositivo, resposta);

        }

        private void EnviarData(Dispositivo dispositivo, string msg)
        {
            if (dispositivo.SerialPort.IsOpen)
                dispositivo.SerialPort.Write(msg);
            else
                throw new Exception("Dispositivo desconectado!");
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                var serial = sender as System.IO.Ports.SerialPort;
                var device = _dispositivos.Where(c =>
                    c.Porta == serial.PortName &&
                    c.Estado == EstadoDispositivo.Desconectar.ToString()).FirstOrDefault();
                if (device == null || device.SerialPort == null)
                    throw new Exception("Dispositivo não foi reconhecido. Por favor renicie à conexão.");

                RxString = device.SerialPort.ReadExisting();
                trataDadoRecebido(device);
            }
            catch (Exception ex)
            {
                ExibirMensagemErro(ex.Message);
            }

        }

        private void Atualizar_Click(Dispositivo dispositivo)
        {
            textBoxDispositivo.Text = dispositivo.Descricao;
            for (int i = 0; i < comboBoxPortas.Items.Count; i++)
            {
                if (comboBoxPortas.Items[i].ToString() == dispositivo.Porta)
                    comboBoxPortas.SelectedIndex = i;
            }
            textBoxChave.Text = dispositivo.ProjetoKey;
            _dispositivo = dispositivo;
            buttonSalvar.Text = "Editar";
            buttonNovo.Enabled = true;
        }
        private void Reset()
        {
            textBoxDispositivo.Text = string.Empty;
            comboBoxPortas.SelectedIndex = -1;
            textBoxChave.Text = string.Empty;
            _dispositivo = null;
            buttonSalvar.Text = "Adicionar";
            buttonNovo.Enabled = false;
        }

        private void AtualizarGrid()
        {

            GridViewDispositivos.DataSource = _dispositivos.ToList();
            for (int i = 0; i < _dispositivos.Count; i++)
            {
                _dispositivos[i].setEstado(0);
                if (_dispositivos[i].Estado == EstadoDispositivo.Conectar.ToString())
                    GridViewDispositivos.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                else
                    GridViewDispositivos.Rows[i].DefaultCellStyle.BackColor = Color.Green;
            }
            GridViewDispositivos.DataSource = _dispositivos.ToList();
        }
        
        private void GridViewDispositivos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.ColumnIndex < 2)
                    return;
                var dispositivo = _dispositivos[e.RowIndex];
                switch (e.ColumnIndex)
                {
                    case 2:

                        if (dispositivo.Estado == EstadoDispositivo.Desconectar.ToString())
                        {
                            if (dispositivo.SerialPort != null)
                            {
                                if (dispositivo.SerialPort.IsOpen)
                                    dispositivo.SerialPort.Close();
                                ExibirMensagemSucesso(MensagemServico.GetMessage(MensagemSistemaEnum.DeviceDisconnected));
                            }
                            dispositivo.setEstado(0);
                            GridViewDispositivos.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Gray;
                            ExibirMensagemSucesso(MensagemServico.GetMessage(MensagemSistemaEnum.NoDeviceConnection));
                        }
                        else
                        {
                            if (Conectar(dispositivo))
                            {
                                dispositivo.SerialPort.Write("0");
                                dispositivo.setEstado(1);
                                GridViewDispositivos.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Green;
                                ExibirMensagemSucesso(MensagemServico.GetMessage(MensagemSistemaEnum.DeviceConnected));
                            }
                        }
                        break;
                    case 3:
                        Atualizar_Click(dispositivo);
                        break;
                    case 4:
                        DialogResult confirm = MessageBox.Show("Deseja deletar dispositivo?", "Dispositivo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                        if (confirm == DialogResult.No)
                            return;
                        if (GridViewDispositivos.CurrentRow != null)
                        {
                            _dispositivos.RemoveAt(GridViewDispositivos.CurrentRow.Index);
                            AtualizarGrid();
                        }
                        ExibirMensagemSucesso(MensagemServico.GetMessage(MensagemSistemaEnum.DeviceDeleteSuccessfully));
                        break;
                }                
            }
            catch (Exception ex)
            {
                ExibirMensagemErro(ex.Message);
            }
        }
        private void buttonNovo_Click(object sender, EventArgs e)
        {
            Reset();
        }
        internal void ExibirMensagemErro(string msg)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(ExibirMensagemErro), msg);
                return;
            }

            this.labelMensagem.ForeColor = Color.Red;
            this.labelMensagem.Text = msg;
            StartTime();
        }

        internal void ExibirMensagemSucesso(string msg)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(ExibirMensagemSucesso), msg);
                return;
            }
            this.labelMensagem.ForeColor = Color.Green;
            this.labelMensagem.Text = msg;
            StartTime();
        }

        static Timer Timer { get; set; }
        static short CountSecond { get; set; }

        public void StartTime()
        {
            if (Timer == null)
            {
                Timer = new Timer();
                Timer.Interval = 5000;
                Timer.Tick += Timer_Tick;
            }
            Timer.Start();
        }

        void Timer_Tick(object sender, EventArgs e)
        {
            CountSecond++;

            if (CountSecond >= 4)
            {
                Timer.Stop();
                this.labelMensagem.Text = string.Empty;
                CountSecond = 0;
            }
        }
    }
}