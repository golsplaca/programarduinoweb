﻿using System;

namespace WindowsFormsApp2
{
    partial class LoginForm
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.labelLogin = new System.Windows.Forms.Label();
            this.textBoxSenha = new System.Windows.Forms.TextBox();
            this.buttonSalvar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxUsuario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.linkEsqueciSenha = new System.Windows.Forms.LinkLabel();
            this.linkCadastroUsuario = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.linkPage = new System.Windows.Forms.LinkLabel();
            this.labelMensagem = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelLogin
            // 
            this.labelLogin.Font = new System.Drawing.Font("Microsoft YaHei Light", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.Location = new System.Drawing.Point(12, 19);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(307, 26);
            this.labelLogin.TabIndex = 5;
            this.labelLogin.Text = "Insira as credênciais para acessar o sistema";
            // 
            // textBoxSenha
            // 
            this.textBoxSenha.Location = new System.Drawing.Point(74, 91);
            this.textBoxSenha.Name = "textBoxSenha";
            this.textBoxSenha.Size = new System.Drawing.Size(129, 20);
            this.textBoxSenha.TabIndex = 7;
            this.textBoxSenha.UseSystemPasswordChar = true;
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(128, 121);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(75, 23);
            this.buttonSalvar.TabIndex = 8;
            this.buttonSalvar.Text = "Entrar";
            this.buttonSalvar.UseVisualStyleBackColor = true;
            this.buttonSalvar.Click += new System.EventHandler(this.btEntrar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Usuário:";
            // 
            // textBoxUsuario
            // 
            this.textBoxUsuario.Location = new System.Drawing.Point(74, 60);
            this.textBoxUsuario.Name = "textBoxUsuario";
            this.textBoxUsuario.Size = new System.Drawing.Size(129, 20);
            this.textBoxUsuario.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Senha:";
            // 
            // linkEsqueciSenha
            // 
            this.linkEsqueciSenha.AutoSize = true;
            this.linkEsqueciSenha.Location = new System.Drawing.Point(14, 128);
            this.linkEsqueciSenha.Name = "linkEsqueciSenha";
            this.linkEsqueciSenha.Size = new System.Drawing.Size(89, 13);
            this.linkEsqueciSenha.TabIndex = 14;
            this.linkEsqueciSenha.TabStop = true;
            this.linkEsqueciSenha.Text = "Esqueci à senha!";
            this.linkEsqueciSenha.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEsqueciSenha_LinkClicked);
            // 
            // linkCadastroUsuario
            // 
            this.linkCadastroUsuario.AutoSize = true;
            this.linkCadastroUsuario.Location = new System.Drawing.Point(14, 162);
            this.linkCadastroUsuario.Name = "linkCadastroUsuario";
            this.linkCadastroUsuario.Size = new System.Drawing.Size(110, 13);
            this.linkCadastroUsuario.TabIndex = 15;
            this.linkCadastroUsuario.TabStop = true;
            this.linkCadastroUsuario.Text = "Sou um novo usuário.";
            this.linkCadastroUsuario.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEsqueciSenha_LinkClicked);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(231, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 51);
            this.label1.TabIndex = 16;
            this.label1.Text = "Com o Smart Serial Connect Ladder é fácil e prático o desenvolvimento dos seus pr" +
    "ojetos embarcados.";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(231, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 16);
            this.label4.TabIndex = 17;
            this.label4.Text = "Dúvidas, acesse:";
            // 
            // linkPage
            // 
            this.linkPage.AutoSize = true;
            this.linkPage.Location = new System.Drawing.Point(231, 144);
            this.linkPage.Name = "linkPage";
            this.linkPage.Size = new System.Drawing.Size(123, 13);
            this.linkPage.TabIndex = 18;
            this.linkPage.TabStop = true;
            this.linkPage.Text = "www.smartladder.com.br";
            this.linkPage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEsqueciSenha_LinkClicked);
            // 
            // labelMensagem
            // 
            this.labelMensagem.ForeColor = System.Drawing.Color.Red;
            this.labelMensagem.Location = new System.Drawing.Point(138, 177);
            this.labelMensagem.Name = "labelMensagem";
            this.labelMensagem.Size = new System.Drawing.Size(275, 27);
            this.labelMensagem.TabIndex = 19;
            this.labelMensagem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoginForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(425, 213);
            this.Controls.Add(this.labelMensagem);
            this.Controls.Add(this.linkPage);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkCadastroUsuario);
            this.Controls.Add(this.linkEsqueciSenha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxUsuario);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.textBoxSenha);
            this.Controls.Add(this.labelLogin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(441, 252);
            this.MinimumSize = new System.Drawing.Size(441, 252);
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login Smart Serial Connect Ladder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.TextBox textBoxSenha;
        private System.Windows.Forms.Button buttonSalvar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkEsqueciSenha;
        private System.Windows.Forms.LinkLabel linkCadastroUsuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkPage;
        private System.Windows.Forms.Label labelMensagem;
    }
}

