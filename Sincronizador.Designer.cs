﻿namespace WindowsFormsApp2
{
    partial class Sincronizador
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sincronizador));
            this.comboBoxPortas = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNomeDispositivo = new System.Windows.Forms.Label();
            this.buttonSalvar = new System.Windows.Forms.Button();
            this.labelKey = new System.Windows.Forms.Label();
            this.GridViewDispositivos = new System.Windows.Forms.DataGridView();
            this.dispositivoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxDispositivo = new System.Windows.Forms.TextBox();
            this.textBoxChave = new System.Windows.Forms.TextBox();
            this.buttonNovo = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.labelMensagem = new System.Windows.Forms.Label();
            this.descricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.portaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Editar = new System.Windows.Forms.DataGridViewImageColumn();
            this.Remover = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDispositivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dispositivoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxPortas
            // 
            this.comboBoxPortas.FormattingEnabled = true;
            this.comboBoxPortas.Location = new System.Drawing.Point(125, 12);
            this.comboBoxPortas.Name = "comboBoxPortas";
            this.comboBoxPortas.Size = new System.Drawing.Size(70, 21);
            this.comboBoxPortas.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(195, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Atualizar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Porta:";
            // 
            // labelNomeDispositivo
            // 
            this.labelNomeDispositivo.AutoSize = true;
            this.labelNomeDispositivo.Location = new System.Drawing.Point(9, 56);
            this.labelNomeDispositivo.Name = "labelNomeDispositivo";
            this.labelNomeDispositivo.Size = new System.Drawing.Size(110, 13);
            this.labelNomeDispositivo.TabIndex = 5;
            this.labelNomeDispositivo.Text = "Nome do  Dispositivo:";
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(277, 74);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(75, 23);
            this.buttonSalvar.TabIndex = 8;
            this.buttonSalvar.Text = "Adicionar";
            this.buttonSalvar.UseVisualStyleBackColor = true;
            this.buttonSalvar.Click += new System.EventHandler(this.btEnviar_Click);
            // 
            // labelKey
            // 
            this.labelKey.AutoSize = true;
            this.labelKey.Location = new System.Drawing.Point(9, 82);
            this.labelKey.Name = "labelKey";
            this.labelKey.Size = new System.Drawing.Size(92, 13);
            this.labelKey.TabIndex = 9;
            this.labelKey.Text = "Chave do Projeto:";
            // 
            // GridViewDispositivos
            // 
            this.GridViewDispositivos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GridViewDispositivos.AutoGenerateColumns = false;
            this.GridViewDispositivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewDispositivos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.descricaoDataGridViewTextBoxColumn,
            this.portaDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.Editar,
            this.Remover});
            this.GridViewDispositivos.DataSource = this.dispositivoBindingSource;
            this.GridViewDispositivos.Location = new System.Drawing.Point(12, 105);
            this.GridViewDispositivos.Name = "GridViewDispositivos";
            this.GridViewDispositivos.RowHeadersVisible = false;
            this.GridViewDispositivos.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.GridViewDispositivos.ShowCellErrors = false;
            this.GridViewDispositivos.ShowCellToolTips = false;
            this.GridViewDispositivos.ShowEditingIcon = false;
            this.GridViewDispositivos.ShowRowErrors = false;
            this.GridViewDispositivos.Size = new System.Drawing.Size(420, 220);
            this.GridViewDispositivos.TabIndex = 10;
            this.GridViewDispositivos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewDispositivos_CellClick);
            // 
            // dispositivoBindingSource
            // 
            this.dispositivoBindingSource.DataSource = typeof(WindowsFormsApp2.Dispositivo);
            // 
            // textBoxDispositivo
            // 
            this.textBoxDispositivo.Location = new System.Drawing.Point(125, 49);
            this.textBoxDispositivo.Name = "textBoxDispositivo";
            this.textBoxDispositivo.Size = new System.Drawing.Size(307, 20);
            this.textBoxDispositivo.TabIndex = 11;
            // 
            // textBoxChave
            // 
            this.textBoxChave.Location = new System.Drawing.Point(125, 75);
            this.textBoxChave.Name = "textBoxChave";
            this.textBoxChave.Size = new System.Drawing.Size(145, 20);
            this.textBoxChave.TabIndex = 12;
            // 
            // buttonNovo
            // 
            this.buttonNovo.Enabled = false;
            this.buttonNovo.Location = new System.Drawing.Point(357, 74);
            this.buttonNovo.Name = "buttonNovo";
            this.buttonNovo.Size = new System.Drawing.Size(75, 23);
            this.buttonNovo.TabIndex = 14;
            this.buttonNovo.Text = "Cancelar";
            this.buttonNovo.UseVisualStyleBackColor = true;
            this.buttonNovo.Click += new System.EventHandler(this.buttonNovo_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.FillWeight = 50F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::WindowsFormsApp2.Properties.Resources.icons8_editar_24;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.Width = 50;
            // 
            // labelMensagem
            // 
            this.labelMensagem.ForeColor = System.Drawing.Color.Red;
            this.labelMensagem.Location = new System.Drawing.Point(12, 327);
            this.labelMensagem.Name = "labelMensagem";
            this.labelMensagem.Size = new System.Drawing.Size(420, 19);
            this.labelMensagem.TabIndex = 20;
            this.labelMensagem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // descricaoDataGridViewTextBoxColumn
            // 
            this.descricaoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.descricaoDataGridViewTextBoxColumn.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.Name = "descricaoDataGridViewTextBoxColumn";
            this.descricaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // portaDataGridViewTextBoxColumn
            // 
            this.portaDataGridViewTextBoxColumn.DataPropertyName = "Porta";
            this.portaDataGridViewTextBoxColumn.FillWeight = 60F;
            this.portaDataGridViewTextBoxColumn.HeaderText = "Porta";
            this.portaDataGridViewTextBoxColumn.Name = "portaDataGridViewTextBoxColumn";
            this.portaDataGridViewTextBoxColumn.ReadOnly = true;
            this.portaDataGridViewTextBoxColumn.Width = 60;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.FillWeight = 80F;
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Ação";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.estadoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.estadoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.estadoDataGridViewTextBoxColumn.Text = "";
            this.estadoDataGridViewTextBoxColumn.Width = 80;
            // 
            // Editar
            // 
            this.Editar.FillWeight = 20F;
            this.Editar.HeaderText = "";
            this.Editar.Image = global::WindowsFormsApp2.Properties.Resources.icons8_editar_15;
            this.Editar.Name = "Editar";
            this.Editar.ReadOnly = true;
            this.Editar.Width = 20;
            // 
            // Remover
            // 
            this.Remover.FillWeight = 20F;
            this.Remover.HeaderText = "";
            this.Remover.Image = global::WindowsFormsApp2.Properties.Resources.icons8_fechar_janela_15;
            this.Remover.Name = "Remover";
            this.Remover.ReadOnly = true;
            this.Remover.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Remover.Width = 20;
            // 
            // Sincronizador
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(446, 348);
            this.Controls.Add(this.labelMensagem);
            this.Controls.Add(this.buttonNovo);
            this.Controls.Add(this.textBoxChave);
            this.Controls.Add(this.textBoxDispositivo);
            this.Controls.Add(this.GridViewDispositivos);
            this.Controls.Add(this.labelKey);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.labelNomeDispositivo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxPortas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(462, 387);
            this.MinimumSize = new System.Drawing.Size(462, 387);
            this.Name = "Sincronizador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insira as credênciais para acessar o sistema";
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDispositivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dispositivoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxPortas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNomeDispositivo;
        private System.Windows.Forms.Button buttonSalvar;
        private System.Windows.Forms.Label labelKey;
        private System.Windows.Forms.DataGridView GridViewDispositivos;
        private System.Windows.Forms.TextBox textBoxDispositivo;
        private System.Windows.Forms.TextBox textBoxChave;
        private System.Windows.Forms.Button buttonNovo;
        private System.Windows.Forms.BindingSource dispositivoBindingSource;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Label labelMensagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn portaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn Editar;
        private System.Windows.Forms.DataGridViewImageColumn Remover;
    }
}

