﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsFormsApp2
{
    public static class SerialRequest
    {

        public static Uri baseUrl = new Uri("http://localhost/");
        public static string Set(string serial)
        {

            var client = new RestSharp.RestClient(baseUrl);
            var request = new RestRequest("ladder_web/app", Method.GET);
            request.AddHeader("Content-Type", "application/json");

            //request.AddJsonBody(incentivo);

            request.AddObject(new { Serial = serial, Buscar = "teste" });
            ;
            var response = client.Get<object>(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return response.Content;
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                throw new Exception("");
            else if (response.StatusCode == 0)
                throw new Exception("");
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                throw new Exception("");
            else
                throw new Exception("");
        }
        public static string Get()
        {
            var client = new RestSharp.RestClient(baseUrl);
            var request = new RestRequest("ladder_web/app", Method.GET);
            request.AddHeader("Content-Type", "application/json");

            //request.AddJsonBody(incentivo);

            request.AddObject(new { });
            ;
            var response = client.Get<object>(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return response.Content;
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                throw new Exception("");
            else if (response.StatusCode == 0)
                throw new Exception("");
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                throw new Exception("");
            else
                throw new Exception("");
        }

        internal static List<Dispositivo> Entrar(string login, string senha)
        {
            LoginUser.Login = login;
            LoginUser.Senha = senha;
            return ObterDispositivos();
        }

        public static List<Dispositivo> ObterDispositivos()
        {
            var client = new RestSharp.RestClient(baseUrl);
            var request = new RestRequest("ladder_web/app", Method.GET);
            request.AddHeader("Content-Type", "application/json");


            var data = new List<Dispositivo>();
            data.Add(new Dispositivo
            {
                Porta = "COM3",
                UsuarioId = 8,
                Descricao = "Arduino Uno",
                ProjetoKey = "FSA4F354425XSEHG3S5"
            });
            data.Add(new Dispositivo
            {
                Porta = "COM8",
                UsuarioId = 8,
                Descricao = "Arduino Mega",
                ProjetoKey = "A4F354425XSEHG3S5"
            });
            return data;
            /*
            //request.AddJsonBody(incentivo);

            request.AddObject(new { User = LoginUser.Login, Password = LoginUser.Senha });
            ;
            var response = client.Get<List<Dispositivo>>(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return response.Data;
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                throw new Exception("");
            else if (response.StatusCode == 0)
                throw new Exception("");
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                throw new Exception("");
            else
                throw new Exception("");
                */
        }

        internal static void EditarDispositivo(Dispositivo device)
        {
            
        }
    }
}