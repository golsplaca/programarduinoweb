﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsFormsApp2.Localizacoes
{
    public enum MensagemSistemaEnum
    {
        None = -1,
        ChooseASerialPort,
        CouldNotChangeDevice,
        CouldNotDeleteDevice,
        DeviceAddedSuccessfully,
        DeviceConnected,
        DeviceDeleteSuccessfully,
        DeviceDisconnected,
        DeviceUpdatedSuccessfully,
        NoConnection,
        NoConnectionToService,
        NoDeviceConnection,
        UnableToSaveDevice,
        UserOrPasswordAreIncorrect
    }
}
