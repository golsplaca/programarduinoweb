﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Text;

namespace WindowsFormsApp2.Localizacoes
{
    public class MensagemServico
    {
        static volatile ResourceManager instanceMessages;
        static object syncRootMessages = new object();

        //static volatile ResourceManager instanceLabel;
        static object syncRootLabel = new object();
        static ResourceManager Message
        {
            get
            {
                if (instanceMessages == null)
                {
                    lock (syncRootMessages)
                    {
                        if (instanceMessages == null)
                            instanceMessages = new ResourceManager(
                                "WindowsFormsApp2.Resources.MensagensSistema",
                                new MensagemSistemaEnum().GetType().Assembly);
                    }
                }
                return instanceMessages;
            }
        }
        public static string GetMessage(MensagemSistemaEnum message, CultureInfo culture = null)
        {
            var currentCulture = System.Globalization.CultureInfo.CurrentUICulture;
            return Message.GetString(message.ToString(), culture ?? currentCulture);
        }
    }
}
